#!/bin/sh

BLOGPATH="" #Enter blog path here without the trailing /
FEEDTITLE="" #Enter your feed's title here
SITEURL="" #Enter the base url of your site here without the trailing / (https://mysite.com)
FEEDDESCRIPTION="" #Enter your feed's description here

cat > ${BLOGPATH}/feed.xml <<EOF
<rss version="2.0">
  <channel>
    <title>${FEEDTITLE}</title>
    <link>${SITEURL}/cgi-bin/blog.cgi</link>
    <description>${FEEDDESCRIPTION}</description>
    <generator>rss.sh -- the rss generator for blog.sh</generator>
    <language>en-us</language>
    <lastBuildDate>$(date)</lastBuildDate>
EOF

COUNTER=0
for ARTICLE in $(cat ${BLOGPATH}/index); do
  echo $ARTICLE | awk -F"," '{print $1" "$2" "$3}' | { read DATE TITLE ID
  TITLE=$(echo $TITLE | tr "_" " ")

  ARTICLETEXT="$(cat ${BLOGPATH}/${ID}.html | lynx -stdin -dump | sed 's/^ *//g')"

  cat >> ${BLOGPATH}/feed.xml <<EOF
    <item>
      <title>${TITLE}</title>
      <link>
        ${SITEURL}/cgi-bin/blogview.sh?id=${ID}
      </link>
      <pubDate>$(date -Rd ${DATE})</pubDate>
      <guid>
        ${SITEURL}/cgi-bin/blogview.sh?id=${ID}
      </guid>
      <description>
        ${ARTICLETEXT}
      </description>
    </item>
EOF
}
COUNTER=$(echo "$COUNTER + 1" | bc)
[ "$COUNTER" -eq "20" ] && break
done

cat >> ${BLOGPATH}/feed.xml <<EOF
  </channel>
</rss>
EOF
