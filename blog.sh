#!/bin/sh

printf "Content-Type: text/html\n\n"

MAX_ITEMS_PER_PAGE=5
BLOGPATH="" #Enter full path to your blog directory here without a trailing /

if [ $QUERY_STRING ]; then
  PAGE=$(echo "${QUERY_STRING#page=} - 1" | bc)
else
  PAGE=0
fi
CPAGE=$(echo "$PAGE + 1" | bc)
NPAGE=$(echo "$PAGE + 2" | bc)

PSTARTLINE=$(echo "$MAX_ITEMS_PER_PAGE * $PAGE" | bc)
STARTLINE=$(echo "$PSTARTLINE + 1" | bc)
ENDLINE=$(echo "$PSTARTLINE + $MAX_ITEMS_PER_PAGE" | bc)
ENDLINEP=$(echo "$ENDLINE + 1" | bc)
COUNTER=0


BDISPLAY="hidden"
[ $PAGE -gt 0 ] && BDISPLAY="visible"
NDISPLAY="visible"
[ "$(head -${ENDLINE} ${BLOGPATH}/index | tail -n +${ENDLINE})" = "$(head -${ENDLINEP} ${BLOGPATH}/index | tail -n +${ENDLINE})" ] && NDISPLAY="hidden"

cat <<EOF
<!-- Write the HTML for everything in your webpage above the article content here -->
<!-- The following lines are included as examples of how to use the navigation button links and page indicator -->

<a style="visibility:${BDISPLAY}" href="/cgi-bin/blog.sh?page=${PAGE}">←</a> Articles <a style="visibility:${NDISPLAY}" href="/cgi-bin/blog.sh?page=${NPAGE}">→</a>
<p>Page $CPAGE</p>
EOF

for ARTICLE in $(tail -n +$STARTLINE ${BLOGPATH}/index); do
  echo $ARTICLE | awk -F"," '{print $1" "$2" "$3}' | { read DATE TITLE ID
  TITLE=$(echo $TITLE | tr "_" " ")

  ARTICLETEXT="$(cat ${BLOGPATH}/${ID}.html)"

  cat <<EOF
    <style>
      #${ID}Container {
        display:none;
      }
      #${ID}Box:checked + #${ID}Container {
        display:block;
      }
      #${ID}Box {
        display:none;
      }
      #${ID}Link {
        user-select:none;
      }
    </style>
    <div class="textcontainer"><label for="${ID}Box" id="${ID}Link" class="articleLink">${DATE} - ${TITLE}</label></div>
    <input type="checkbox" id="${ID}Box">
    <div id="${ID}Container"><div class="textcontainer"><div class="textcontent">${ARTICLETEXT}</div></div></div>
EOF
}
COUNTER=$(echo "$COUNTER + 1" | bc)
[ "$COUNTER" -eq "$MAX_ITEMS_PER_PAGE" ] && break
done

cat <<EOF
    <!-- Add footer HTML or anything that goes below the article content here -->

    <div class="textcontainer"><div class="footercontainer"><p class="footertext">Powered by blog.sh</p></div></div>
  </body>
</html>
EOF
