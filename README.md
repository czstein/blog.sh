# blog.sh

blog.sh is a minimalist, javascript-free blog framework written in POSIX shell that runs as a cgi script on your web server.

## Features

- Automatically generates blog pages with clickable titles that expand post contents using pure CSS
- Generates an RSS feed with links to show each post on its own page
- Easy to set the number of posts that load on each page
- Blog posts support formatting and images through HTML, and full styling support with CSS

## Dependencies

 - Web server with CGI capability
 - bc for calculations in blog.cgi
 - lynx for rss.sh (but this could be removed easily if you don't want to pre-render your posts as text for the RSS feed)

## How to use

First, create a directory on your server that will contain your blog posts and index. Create a file called index and use the syntax "date,articleTitle,articleID" for each line. For the article title, all underscores will be replaced with spaces, so use underscores in the index file since using spaces will mess up the parsing of it. Next, create an HTML file in your blog directory named exactly with the articleID with a .html extension, and write your blog post in that file using any HTML formatting you want. Don't inclue head or body tags, or anything else like that. This file will be injected as is into your main blog page, so all you need to add here is specific formatting. Any CSS file linked to on the main blog page will affect the articles, so feel free to add classes and style your blog posts as you please. Then, add the blog.cgi file to your cgi-bin directory; fill in the MAX_ITEMS_PER_PAGE and BLOGPATH variables, making sure not to include a trailing / in the BLOGPATH; and add your desired HTML content for the main blog page in the two sections for it, above and below the article content. Finally, it should all be working at this point, go test it out!

Also, check out what a working example looks like over at https://steinworks.tech/cgi-bin/blog.sh

## To-Do

  - Add screenshots and generally improve the How to use section
