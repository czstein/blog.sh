#!/bin/sh

printf "Content-Type: text/html\n\n"

BLOGPATH="" #Enter the path to your blog directory without the trailing /

BLOGPOST="${QUERY_STRING#id=}"
for ARTICLE in $(cat ${BLOGPATH}/index); do
  echo $ARTICLE | awk -F"," '{print $1" "$2" "$3}' | { read DATE TITLE ID
  TITLE=$(echo $TITLE | tr "_" " ")

  if [ "$BLOGPOST" = "$ID" ]; then
    ARTICLETEXT="$(cat ${BLOGPATH}/${ID}.html)"

    cat <<EOF
    <!-- Enter your HTML here -->
    <!-- The following lines are an example of how to use the TITLE, DATE, and ARTICLETEXT variables -->

    <!DOCTYPE html>
    <html lang="en">
  	  <body style="padding-bottom:40px">
        <h1>${TITLE}</h1>
        <h3>${DATE}</h3>
        <div class="textcontainer"><div class="textcontent">${ARTICLETEXT}</div></div>
        <div class="textcontainer"><div class="footercontainer"><p class="footertext">Powered by <a target="_blank" href="https://gitlab.com/czstein/blog.sh">blog.sh</a></p></div></div>
      </body>
    </html>
EOF
    break
  fi
}
done
